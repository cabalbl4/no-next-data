# This project is a working prototype to bypass next.js __NEXT_DATA__ by using redis

local redis on default port is required (use docker one for example)

`docker run --name redis -p 6379:6379 -d redis`

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```


Open [http://localhost:3000/test](http://localhost:3000/test) with your browser to see the result.

