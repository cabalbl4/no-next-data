// Next.js API route support: https://nextjs.org/docs/api-routes/introduction
import type { NextApiRequest, NextApiResponse } from 'next'
import { redisClient } from '../../../props-store/redis-client';


export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  const { id, format = 'js' } = req.query;
  const data =  await redisClient.get(`${id}`);
  if(! data) {
    res.status(404).setHeader("Content-type", "text/javascript")
    .json({ status: 'NOT FOUND' });
  } else {
    if(format === 'js') {
      res.status(200).setHeader("Content-type", "text/javascript").send(`
      window.__NEXT_BYPASS_PROPS__  = ${data}
    `);
    } else {
      res.status(200).setHeader("Content-type", "text/json").send(data);
    }
  }
}
