import * as React from 'react';
import type { NextPage } from 'next'
import Head from 'next/head'
import { LoremIpsum } from "lorem-ipsum";
import styles from '../styles/Home.module.css'

import largeServerSideData from '../large-data-test.json'
import { storeNextProps } from '../props-store/server'
import Link  from 'next/link';
import { ClientRefreshProvider, getNextPropsClient, ClientDataContext } from '../props-store/client'

const ssrData: any = {};


const RefreschableContextUser: React.FC = () => {
  // Here is where we use our redis-stored "next data"
  /* eslint-disable */
  const data = React.useContext(ClientDataContext);
  return <> 
  <div className={styles.container}>
    <Head>
      <title>Create Next App</title>
      <meta name="description" content="Generated by create next app" />
      <link rel="icon" href="/favicon.ico" />
    </Head>
    <h1>Here is some large text from props</h1>

    <div style={{display: 'flex', flexDirection: 'column', textDecoration: 'underline'}}>
    <a href="/test1">test 1 - server side</a>
    <a href="/test2">test 2 - server side</a>
    <Link href="/test3"><a>test 3 - client side</a></Link>
    <Link href="/test4"><a>test 4 - client side</a></Link>
    <Link href="/">GO TO INDEX</Link>
    </div>
    <div>
    {JSON.stringify(data)}
    </div>

  </div>
  </>
}

const Home: NextPage<{id: string, ssrGetter: () => string}> = ({ id }) => {
  const ssrProps = ssrData[id];
  // do not leak
  delete ssrData[id];
  return (
    <ClientRefreshProvider id={id} ssrData={ssrProps}>
     <div>{id}</div>
     <RefreschableContextUser></RefreschableContextUser>
    </ClientRefreshProvider>
  )
}

export default Home

function createBigPayloadData(): any {
  const dataToSave = JSON.parse(JSON.stringify(largeServerSideData));

  const lorem = new LoremIpsum({
    sentencesPerParagraph: {
      max: 8,
      min: 4
    },
    wordsPerSentence: {
      max: 16,
      min: 4
    }
  });
  (dataToSave as any).dynamicProps = lorem.generateParagraphs(20);
  return dataToSave;

}


export async function getServerSideProps({req}: any) {
  // Just pretend we got a lot of data
  const dataToSave = createBigPayloadData();
  
  dataToSave.url = req.url; 
  // Here we actually save, both for seever and client
  const id = await storeNextProps(dataToSave);
  ssrData[id] = dataToSave;
  return {
    props: {
      id,
    }
  }
};
 
