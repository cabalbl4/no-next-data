import * as React from 'react';
import Link  from 'next/link';


export default function nextPage()  {
  return <Link href="/test">GO TO TEST PAGE (WILL TRIGGER RENDER PROBLEM)</Link>
}