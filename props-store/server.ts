import { v4 } from 'uuid';
import { redisClient } from './redis-client';

export async function storeNextProps(data: any): Promise<string> {
    const id = v4();
    data.serverId = id;
    console.log('Store props for', id);
    await redisClient.set(id, JSON.stringify(data), { 
        // No client should take longer to load than those
        EX: 60
    })
    return id;
}