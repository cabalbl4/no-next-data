import * as React from "react";

export function getNextPropsClient() {
    return (window as any).__NEXT_BYPASS_PROPS__;
}

export const ClientDataContext = React.createContext({});



export const ClientRefreshProvider: React.FC<{children: any, id: string, ssrData: any}> = ({ children, id, ssrData }) => {
    /* eslint-disable */
    // Trigger refresh on props update
    const [value, setValue] = React.useState(ssrData || getNextPropsClient())
    React.useEffect(() => {
        fetch(`/api/next-data/${id}?format=json`)
        .then(res => res.json())
        .then(setValue)
        .catch(console.error);
    }, [id]);

    // Re-render on page transition if previos page has no props bypass
    if(! value) {
        return <script id="initlal-bypass" src={`/api/next-data/${id}?format=js`} onLoad={() => setValue(getNextPropsClient())}></script>
    }

    value.id = id;
    return <ClientDataContext.Provider value={value}>
        {children}
        <script id="initlal-bypass" src={`/api/next-data/${id}?format=js`} ></script>
      </ClientDataContext.Provider>;
}
