import { createClient } from 'redis';

// Define redis properly here

export const redisClient = createClient();

redisClient.connect();


redisClient.on('error', (err) => console.log('Redis Client Error', err));

